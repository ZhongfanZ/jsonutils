#include "JsonObject.h"

JsonObject::JsonObject()
{
	// nothing to do
}

JsonObject::JsonObject(const std::string& jsonString)
{
	if (! loadFromString(jsonString))
		printf("failed failed to load json string\n");
}

bool JsonObject::loadFromString(const std::string& jsonString)
{
	if (! validate(jsonString))
		return false;

	return true;
}

JsonMap JsonObject::data() const
{
	return m_data;
}

bool JsonObject::validate(const std::string &jsonString)
{
	if (jsonString.front() != '{')
		return false;

	// first pass, match braces
	int currentBraceLevel = 0;
	for (const auto& character : jsonString)
	{
		if (character == '{')
			++currentBraceLevel;
		else if (character == '}')
			--currentBraceLevel;

		if (currentBraceLevel < 0)
			return false;
	}

	if (currentBraceLevel > 0)
		return false;

	return true;
}

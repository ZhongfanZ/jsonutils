cmake_minimum_required(VERSION 3.20.1)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# include dir subject to change
set(JsonObject_Include_Dirs ".")

list(APPEND JsonObjectSources
    JsonObject.cpp)

add_library(JsonObject STATIC ${JsonObjectSources})

target_include_directories(JsonObject PUBLIC
    ${JsonObject_Include_Dirs})
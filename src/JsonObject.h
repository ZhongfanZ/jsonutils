#pragma once

#include <string>
#include <map>

using JsonMap = std::map<std::string, std::string>;

class JsonObject
{
public:
	JsonObject();
	JsonObject(const std::string& jsonString);

	bool	loadFromString(const std::string& jsonString);
	JsonMap data() const;
	
private:
	bool	validate(const std::string& jsonString);

private:
	JsonMap m_data;
};
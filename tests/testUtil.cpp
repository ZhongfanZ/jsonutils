#include "testUtil.h"
#include <fstream>
#include <sstream>

std::optional<std::string> Test::Util::readFileIntoString(const std::string& fileName)
{
	std::ifstream textFile(fileName);

	if (! textFile.is_open())
		return std::nullopt;

	std::stringstream buf;
	buf << textFile.rdbuf();

	return buf.str();
}
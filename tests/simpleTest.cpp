#include "testUtil.h"

#include <JsonObject.h>

#include <iostream>

constexpr char* kInvalidOpenBraceTest = "simpleTests/invalidOpenBrace.json";
constexpr char* kInvalidCloseBraceTest = "simpleTests/InvalidCloseBrace.json";
constexpr char* khelloWorldTest = "simpleTests/helloWorldTest.json";

int g_testsDone = 0;

bool doLoadTest(const char* fileName, const bool expected)
{
	++g_testsDone;

	printf("Doing test: %s...\n", fileName);

	const auto testString = Test::Util::readFileIntoString(fileName);

	if (! testString)
	{
		fprintf(stderr, "Test Failed: %s is empty.\n", fileName);
		return false;
	}

	JsonObject jsonObject;

	if (jsonObject.loadFromString(testString.value()) == expected)
	{
		printf("Test: %s PASSED!\n", fileName);
		return true;
	}

	printf("Test: %s FAILED!\n", fileName);
	return false;
}

int main()
{
	int testsPassed = 0;

	testsPassed += doLoadTest(kInvalidOpenBraceTest, false);
	printf("\n");
	testsPassed += doLoadTest(kInvalidCloseBraceTest, false);
	printf("\n");
	testsPassed += doLoadTest(khelloWorldTest, true);
	printf("\n");

	printf("Testing complete! %d/%d tests passed.\n", testsPassed, g_testsDone);

	return 0;
}
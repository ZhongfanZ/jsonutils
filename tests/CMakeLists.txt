cmake_minimum_required(VERSION 3.20.1)
project(JsonUtilsTests)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# bin and include subject to change
set(JsonObject_Include_Dirs "../src/")
set(JsonObject_Bin "../src/")

add_subdirectory(${JsonObject_Include_Dirs} ${JsonObject_Bin})

add_executable(simpleTest 
	simpleTest.cpp
	testUtil.cpp
)

target_link_libraries(simpleTest PRIVATE
	JsonObject
)
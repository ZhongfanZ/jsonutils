#pragma once

#include <string>
#include <optional>

namespace Test{
namespace Util{

std::optional<std::string> readFileIntoString(const std::string& fileName);

}
}